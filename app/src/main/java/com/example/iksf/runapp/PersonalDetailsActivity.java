package com.example.iksf.runapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

public class PersonalDetailsActivity extends BaseActivity {
    private Integer age;
    private Integer weight;
    private Integer height;
    private TextView ageEntry;
    private TextView weightEntry;
    private TextView heightEntry;
    private RadioButton maleRadio;
    private RadioButton femaleRadio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_details);
        ageEntry = findViewById(R.id.personalDetailsAgeEntry);
        weightEntry = findViewById(R.id.personalDetailsWeightEntry);
        heightEntry = findViewById(R.id.personalDetailsHeightEntry);
        maleRadio = findViewById(R.id.personalDetailsMaleRadio);
        femaleRadio = findViewById(R.id.personalDetailsFemaleRadio);

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mUser != null) {
            mDb.collection("user_details").whereEqualTo("user_id", mUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        if (!task.getResult().isEmpty()) {
                            DocumentSnapshot k = task.getResult().getDocuments().get(0);
                            ageEntry.setText(k.getLong("age").toString());
                            heightEntry.setText(k.getLong("height").toString());
                            weightEntry.setText(k.getLong("weight").toString());
                            if (k.getString("gender").equals("Male")) {
                                maleRadio.setChecked(true);
                                femaleRadio.setChecked(false);
                            } else if (k.getString("gender").equals("Female")) {
                                maleRadio.setChecked(false);
                                femaleRadio.setChecked(true);
                            }
                        }
                    }
                }
            });
        }
    }

    public void personalDetailsSubmitButtonClick(View v) {
        Boolean male = maleRadio.isChecked();
        Boolean female = femaleRadio.isChecked();
        String age_str = ageEntry.getText().toString();
        String weight_str = weightEntry.getText().toString();
        String height_str = heightEntry.getText().toString();
        if ((male || female) && (age_str.length() > 0 && weight_str.length() > 0 && height_str.length() > 0)) {
            try {
                age = Integer.parseInt(age_str);
                weight = Integer.parseInt(weight_str);
                height = Integer.parseInt(height_str);
            } catch (NumberFormatException e) {
                cleanupFormAndComplainAboutError();
            }
        }
        String gender;
        if (male) {
            gender = "Male";
        } else {
            gender = "Female";
        }
        if (age != null && height != null && weight != null) {
            Log.d("MyLogger", age + " " + height + " " + weight + " " + gender);
            Map<String, Object> user = new HashMap<>();
            user.put("user_id", mUser.getUid());
            user.put("gender", gender);
            user.put("age", age);
            user.put("weight", weight);
            user.put("height", height);
            updateDatabase(user);
            sendToWelcomeActivity();
        } else {
            Log.d("MyLogger", "Some input validation error");
        }
    }

    private void updateDatabase(final Map<String, Object> map) {

        mDb.collection("user_details").whereEqualTo("user_id", mUser.getUid()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
            @Override
            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                if (queryDocumentSnapshots.size() > 0) {
                    for (DocumentSnapshot doc : queryDocumentSnapshots) {
                        doc.getReference().update(map);
                    }
                } else {
                    mDb.collection("user_details")
                            .add(map)
                            .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                                @Override
                                public void onComplete(@NonNull Task<DocumentReference> task) {
                                    Log.d("MyLogger", "Added entry successfully");
                                    sendToWelcomeActivity();
                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("MyLogger", "Failed to add entry");
                                    cleanupFormAndComplainAboutError();
                                }
                            });

                }

            }
        });
    }

    private void cleanupFormAndComplainAboutError() {
        //unimplemented!
        Log.d("MyLogger", "Some error on form, should be some error prompt here, gunna just clear for now");

        maleRadio.setChecked(false);
        femaleRadio.setChecked(false);
        ageEntry.setText("");
        heightEntry.setText("");
        weightEntry.setText("");
        //to-do
    }
}

package com.example.iksf.runapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class NewRunActivity extends BaseActivity {

    static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    //private Date startTime;
    //private Date mostRecentTime;
    private ArrayList<Location> locations;
    private ArrayList<Date> timestamps;
    //private double distance;
    private Button startButton;
    private Button discardButton;
    private Button finishButton;
    private Optional<Integer> runnerWeight;
    private LocationManager locationManager;
    private TokenGenerator tokenGenerator;
    private LocationListener locationListener;
    private TextView startNewRunLabel;
    private TextView stopNewRunLabel;
    private TextView discardNewRunLabel;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    Log.d("MyLogger", "User must have declined permission, going back to welcome screen");
                    sendToWelcomeActivity();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_run);
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locations = new ArrayList<>();
        timestamps = new ArrayList<>();
        startNewRunLabel = findViewById(R.id.startNewRunLabel);
        stopNewRunLabel = findViewById(R.id.stopNewRunLabel);
        discardNewRunLabel = findViewById(R.id.discardNewRunLabel);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        tokenGenerator = new TokenGenerator();

        mDb.collection("user_details").whereEqualTo("user_id", mUser.getUid()).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot k = task.getResult().getDocuments().get(0);
                    /*
                    ageEntry.setText(k.getLong("age").toString());
                    heightEntry.setText(k.getLong("height").toString());
                    weightEntry.setText(k.getLong("weight").toString());
                    */
                    runnerWeight = Optional.of(k.getLong("weight").intValue());
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        startButton = findViewById(R.id.newRunActivityStartButton);
        finishButton = findViewById(R.id.newRunActivityFinishedButton);
        discardButton = findViewById(R.id.newRunActivityDiscardButton);
        finishButton.setEnabled(false);
        discardButton.setEnabled(false);
        startButton.setEnabled(true);

        startButton.setVisibility(View.VISIBLE);
        finishButton.setVisibility(View.INVISIBLE);
        discardButton.setVisibility(View.INVISIBLE);

        startNewRunLabel.setVisibility(View.VISIBLE);
        stopNewRunLabel.setVisibility(View.INVISIBLE);
        discardNewRunLabel.setVisibility(View.INVISIBLE);
    }

    public void onStartButtonClicked(View v) {
        finishButton.setEnabled(true);
        finishButton.setVisibility(View.VISIBLE);
        discardButton.setEnabled(true);
        discardButton.setVisibility(View.VISIBLE);
        startButton.setEnabled(false);
        startButton.setVisibility(View.INVISIBLE);

        startNewRunLabel.setVisibility(View.INVISIBLE);
        stopNewRunLabel.setVisibility(View.VISIBLE);
        discardNewRunLabel.setVisibility(View.VISIBLE);
        locationListener = new LocationListener() {

            public void onLocationChanged(Location location) {
                locations.add(location);
                //timestamps.add( ;)

                Log.d("MyLogger", "Added point " + location.getLatitude() + " " + location.getLongitude() + " " + location.getTime());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }
        };
        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10_000, 2, locationListener);

            Log.d("MyLogger", "GPS permissions passed ");
        } catch (SecurityException e) {
            Log.d("MyLogger", "GPS permissions error? " + e.toString());
            /*
            todo
            inform user they're stupid for declining to give GPS access
             */
        }

    }

    private long getLengthOfRun() {
        if (locations.size() > 1) {
            return (long) ((locations.get(locations.size() - 1).getTime() - locations.get(0).getTime()) * 1e-3);
        } else {
            return 0;
        }
    }

    private long getDistanceOfRun() {
        long distance = 0;
        Optional<Location> location = Optional.empty();
        for (Location l : locations) {
            if (location.isPresent()) {
                distance += l.distanceTo(location.get());
            } else {
                location = Optional.of(l);
            }
        }
        return distance;
    }

    public void onFinishButtonClicked(View v) {
        locationManager.removeUpdates(locationListener);
        long time = getLengthOfRun();
        long distance = getDistanceOfRun();
        double average_speed = (double) distance / (double) time;
        long cal = 0;
        if (runnerWeight.isPresent()) {
            cal = new RunnerFitness(runnerWeight.get()).caloriesBurnt(distance, time);
        }


        String run_id = tokenGenerator.generateToken(mUser.getUid());
        Map<String, Object> run_meta = new HashMap<>();
        run_meta.put("avg_speed", average_speed);
        run_meta.put("cal", cal);
        run_meta.put("distance", distance);
        run_meta.put("run_id", run_id);
        run_meta.put("time", time);
        run_meta.put("timestamp", new Date());
        run_meta.put("user_id", mUser.getUid());
        ArrayList<Map<String, Object>> points = new ArrayList<>();
        for (Location l : locations) {
            Map<String, Object> point = new HashMap<>();
            point.put("point", new GeoPoint(l.getLatitude(), l.getLongitude()));
            point.put("run_id", run_id);
            Date d = new Date();
            d.setTime(l.getTime());
            point.put("time", d);
            points.add(point);
        }
        updateRunMetaDatabase(run_meta, points);
        sendToWelcomeActivity();
        ;

    }

    private void updateRunMetaDatabase(final Map<String, Object> run_meta, final ArrayList<Map<String, Object>> point_list) {
        mDb.collection("run_metadata").add(run_meta).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
            @Override
            public void onComplete(@NonNull Task<DocumentReference> task) {
                Log.d("MyLogger", "Added run meta successfully");


            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("MyLogger", "Some error adding data");
                /*todo
                error handling here
                 */
            }
        });
        for (Map<String, Object> point : point_list) {
            mDb.collection("run_points").add(point).addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                @Override
                public void onComplete(@NonNull Task<DocumentReference> task) {
                    Log.d("MyLogger", "Added point successfully");
                            /*
                            todo
                            find bulk way to add or is this automagically optimised?
                             */
                }
            });
        }
    }

    public void onDiscardButtonClicked(View v) {
        sendToWelcomeActivity();
    }
}

package com.example.iksf.runapp;

import android.os.Bundle;
import android.view.View;

public class WelcomeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
    }

    public void signOutButton(View v) {
        signOut();
    }

    public void updateDetailsButton(View v) {
        sendToGetDetails();
    }

    public void showRunsButton(View v) {
        sendToRunListActivity();
    }

    public void newRunButton(View v) {
        sendToNewRunActivity();
    }
}

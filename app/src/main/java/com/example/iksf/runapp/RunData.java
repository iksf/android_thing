package com.example.iksf.runapp;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class RunData implements Comparable<RunData> {
    public double avg_speed;
    public double cal;
    public double distance;
    public double duration;
    public String run_id;
    public Timestamp timestamp;
    public Date date_time;
    public ArrayList<RunPoint> points;

    public RunData(double i_speed, double i_calories, double i_distance, double i_duration, String i_run_id, Date i_date_time) {
        this.avg_speed = i_speed;
        this.cal = i_calories;
        this.distance = i_distance;
        this.run_id = i_run_id;
        this.date_time = i_date_time;
        this.duration = i_duration;
        this.timestamp = new Timestamp(i_date_time);
        this.points = new ArrayList<>();
    }

    public void sortList() {
        Collections.sort(points);
    }

    public int compareTo(RunData other) {
        return -timestamp.compareTo(other.timestamp);
    }

    public void populatePointsList(FirebaseFirestore mDb) {
        mDb.collection("run_points").whereEqualTo("run_id", run_id).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    Log.d("MyLogger", "Points:\n");
                    for (DocumentSnapshot doc : task.getResult()) {
                        GeoPoint point = doc.getGeoPoint("point");
                        Timestamp t = new Timestamp(doc.getDate("time"));
                        points.add(new RunPoint(point, t));
                        Log.d("MyLogger", "Run point for id: " + run_id + " " + point.toString() + "\n");
                    }
                    Log.d("MyLogger", "\nEnd of points\n");
                    sortList();
                }
            }
        });
    }

    public ArrayList<LatLng> latLngs() {
        ArrayList<LatLng> l = new ArrayList<>();
        for (RunPoint r : this.points) {
            l.add(new LatLng(r.point.getLatitude(), r.point.getLongitude()));
        }
        return l;
    }
}

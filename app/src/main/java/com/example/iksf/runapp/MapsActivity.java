package com.example.iksf.runapp;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

public class MapsActivity extends Activity implements OnMapReadyCallback {
    private TextView date;
    private TextView distance;
    private TextView speed;
    private TextView cal;
    private TextView time;
    private ArrayList<LatLng> points;
    private Polyline line;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        MapFragment map = new MapFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.mapsActivityContainer, map);
        fragmentTransaction.commit();
        map.getMapAsync(this);
        date = findViewById(R.id.mapsActivityDate);
        distance = findViewById(R.id.mapsActivityDistance);
        speed = findViewById(R.id.mapsActivitySpeed);
        cal = findViewById(R.id.mapsActivityCal);
        time = findViewById(R.id.mapsActivityTime);

    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        date.setText(intent.getStringExtra("date"));
        distance.setText(intent.getStringExtra("distance"));
        speed.setText(intent.getStringExtra("speed"));
        cal.setText(intent.getStringExtra("cal"));
        time.setText(intent.getStringExtra("duration"));
        points = intent.getParcelableArrayListExtra("latlngs");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        PolylineOptions options = new PolylineOptions().width(5).color(Color.RED);
        for (LatLng p : points) {
            options.add(p);
        }
        line = googleMap.addPolyline(options);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points.get(0), 11f));
    }


}
